# Application module
Sample application that uses laman spring boot library modules.

Application module that provide business functionality. 

## Technology stack
* [Spring boot](https://spring.io/projects/spring-boot)

## Module dependency
* [Core](https://github.com/ekamekas/spring-library-core)
* [Authentication](https://github.com/ekamekas/spring-library-authentication)

## How to use
1. Put this module in the root project
2. Add dependency in the root project gradle