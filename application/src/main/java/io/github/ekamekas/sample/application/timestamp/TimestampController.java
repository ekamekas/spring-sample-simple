package io.github.ekamekas.sample.application.timestamp;

import io.github.ekamekas.library.core.delegator.RequestHandler;
import io.github.ekamekas.library.core.dto.ResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
@RequestMapping("api/v1/public/timestamp")
public class TimestampController {

    @GetMapping
    public ResponseEntity<ResponseDTO> getTimestamp(){
        RequestHandler requestHandler = new RequestHandler() {
            @Override
            public ResponseEntity onSuccess() {
                return ResponseEntity.ok(Instant.now().getEpochSecond());
            }
        };
        return requestHandler.delegate();
    }

}
