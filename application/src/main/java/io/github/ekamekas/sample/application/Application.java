package io.github.ekamekas.sample.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan("io.github.ekamekas")
@EnableJpaRepositories("io.github.ekamekas")
@SpringBootApplication(scanBasePackages = {
        "io.github.ekamekas"
})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
