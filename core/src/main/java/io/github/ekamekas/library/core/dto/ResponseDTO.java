/*
 * Developed by Laman [ekamekas@github.com] on 5/30/19 9:25 AM.
 * Last modified 5/30/19 9:17 AM
 */

package io.github.ekamekas.library.core.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.github.ekamekas.library.core.enums.IMessageCode;

import javax.validation.constraints.NotNull;

/**
 * Data transfer object to wrap response interface.
 * @author Laman
 */
@JsonSerialize
public class ResponseDTO {

    @NotNull
    private String code;
    @NotNull
    private String message;
    private ResultDTO result;

    /**
     * This constructor used to create ResponseDTO object
     * @param message Message of response
     */
    public ResponseDTO(@NotNull IMessageCode message) {
        this.code = message.getCode();
        this.message = message.getMessage();
    }

    /**
     * This constructor used to create ResponseDTO object.
     * @param result Result object
     * @see ResultDTO
     */
    public ResponseDTO(ResultDTO result) {
        this.result = result;
    }

    /**
     * This constructor used to create RespnseDTO object.
     * @param message Message code of response
     * @param result Result object
     * @see io.github.ekamekas.library.core.enums.MessageCode
     * @see ResultDTO
     */
    public ResponseDTO(@NotNull IMessageCode message, ResultDTO result) {
        this.code = message.getCode();
        this.message = message.getMessage();
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public ResultDTO getResult() {
        return result;
    }

    public void setResult(ResultDTO result) {
        this.result = result;
    }
}
