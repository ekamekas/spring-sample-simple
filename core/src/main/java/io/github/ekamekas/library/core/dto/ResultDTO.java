/*
 * Developed by Laman [ekamekas@github.com] on 5/30/19 9:18 AM.
 * Last modified 5/30/19 9:18 AM
 */

package io.github.ekamekas.library.core.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Data transfer object to wrap response interface.
 * @author Laman
 */
@JsonSerialize
public class ResultDTO {

    private String id;

    /**
     * This constructor used to create ResultDTO object.
     * @param result Result object
     */
    public ResultDTO(Object result) {
        this.result = result;
    }

    /**
     * This constructor used to create ResultDTO object.
     * @param id Secured id of result
     * @param version Version of result
     */
    public ResultDTO(String id, Integer version) {
        this.id = id;
        this.version = version;
    }

    /**
     * This constructor used to create ResultDTO object.
     * @param id Secured id of result
     * @param version Version of result
     * @param result Result object
     */
    public ResultDTO(String id, Integer version, Object result) {
        this.id = id;
        this.version = version;
        this.result = result;
    }

    private Integer version;
    private Object result;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
