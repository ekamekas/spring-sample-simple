package io.github.ekamekas.library.core.enums;

/**
 * An interface to represent message code.
 * @author Laman
 */
public interface IMessageCode {

    String getCode();
    String getMessage();

}
