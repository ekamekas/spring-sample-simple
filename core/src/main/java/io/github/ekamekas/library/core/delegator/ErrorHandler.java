/*
 * Developed by Laman [ekamekas@github.com] on 5/30/19 11:03 AM.
 * Last modified 5/30/19 11:03 AM
 */

package io.github.ekamekas.library.core.delegator;

import io.github.ekamekas.library.core.dto.ResponseDTO;
import io.github.ekamekas.library.core.enums.IMessageCode;
import io.github.ekamekas.library.core.enums.MessageCode;
import io.github.ekamekas.library.core.exceptions.GenericException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Handling error in the context of Response Entity
 * @author Laman
 * @see org.springframework.http.ResponseEntity
 */
public class ErrorHandler {

    public static ResponseEntity delegate(Throwable throwable){
        if(throwable instanceof GenericException){
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseDTO(((GenericException) throwable).getMessageCode()));
        } else {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ResponseDTO(new IMessageCode() {
                        @Override
                        public String getCode() {
                            return MessageCode.MESSAGE_FAILED.getCode();
                        }

                        @Override
                        public String getMessage() {
                            return throwable.getLocalizedMessage();
                        }
                    }));
        }
    }

}
