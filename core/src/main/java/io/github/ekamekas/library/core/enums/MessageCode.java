/*
 * Developed by Laman [ekamekas@github.com] on 5/30/19 9:15 AM.
 * Last modified 5/30/19 9:14 AM
 */

package io.github.ekamekas.library.core.enums;

/**
 * Message code used to represent system behavior.
 * @author Laman
 * @see IMessageCode
 */
public enum MessageCode implements IMessageCode {
    MESSAGE_DELIVERED("000","Message delivered"),
    MESSAGE_FAILED("XXX","Message failed to process");

    private String code;
    private String message;

    MessageCode(String code, String message){
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
