/*
 * Developed by Laman [ekamekas@github.com] on 5/30/19 11:36 AM.
 * Last modified 5/30/19 11:36 AM
 */

package io.github.ekamekas.library.core.repository;

import io.github.ekamekas.library.core.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity> extends JpaRepository<T,Long> {
    T findBySecureId(String secureId);
}
