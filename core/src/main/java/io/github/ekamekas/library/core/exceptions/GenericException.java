/*
 * Developed by Laman [ekamekas@github.com] on 5/30/19 9:29 AM.
 * Last modified 5/30/19 9:29 AM
 */

package io.github.ekamekas.library.core.exceptions;

import io.github.ekamekas.library.core.enums.IMessageCode;

/**
 * Generic exception that wrap messages
 * @author Laman
 */
public class GenericException extends Exception {

    private IMessageCode messageCode;

    /**
     * This constructor used to create GenericException object.
     * Message data from IMessageCode pass to Exception message argument.
     * @param messageCode Message code to represent system behavior
     * @see IMessageCode
     * @see Exception
     */
    public GenericException(IMessageCode messageCode) {
        super(messageCode.getMessage());
        this.messageCode = messageCode;
    }

    /**
     * This constructor used to create GenericException object.
     * @param message Exception message string
     * @param messageCode Message code to represent system behavior
     * @see IMessageCode
     * @see Exception
     */
    public GenericException(String message, IMessageCode messageCode) {
        super(message);
        this.messageCode = messageCode;
    }

    public IMessageCode getMessageCode() {
        return messageCode;
    }
}
