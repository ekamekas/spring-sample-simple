package io.github.ekamekas.library.core.delegator;

import org.springframework.http.ResponseEntity;

abstract public class RequestHandler {

    public abstract ResponseEntity onSuccess();

    public ResponseEntity delegate(){
        try {
            return onSuccess();
        } catch (Exception e){
            return ErrorHandler.delegate(e);
        }
    }

}
