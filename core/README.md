# Core module
This module is part of laman spring boot library.

Core module for spring boot project. This module provide common codes that uses in other applications/modules.

## Technology stack
* [Spring boot](https://spring.io/projects/spring-boot)

## How to use
1. Put this module in the root project
2. Add dependency in the root project gradle