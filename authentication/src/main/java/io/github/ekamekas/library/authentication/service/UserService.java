package io.github.ekamekas.library.authentication.service;

import io.github.ekamekas.library.authentication.entity.UserEntity;
import io.github.ekamekas.library.authentication.repository.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserEntity userEntity = userRepository.findByUsername(username).orElseThrow();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(userEntity.getRole().getCode());

        return new User(userEntity.getUsername(),userEntity.getPassword(), Collections.singletonList(grantedAuthority));
    }
}
