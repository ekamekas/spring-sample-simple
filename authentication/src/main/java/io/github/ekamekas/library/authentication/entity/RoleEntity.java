package io.github.ekamekas.library.authentication.entity;

import io.github.ekamekas.library.core.entity.BaseEntity;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
@Table(name = "ROLE", indexes = {
        @Index(name = "IDX_ROLE", columnList = "code")
})
public class RoleEntity extends BaseEntity {
    @NonNull
    @Column(nullable = false, unique = true)
    private String code;

    @NonNull
    @Column(nullable = false)
    private String name;

    private String description;

    public RoleEntity() {}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return super.toString() + '\n' +
                "RoleEntity{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
