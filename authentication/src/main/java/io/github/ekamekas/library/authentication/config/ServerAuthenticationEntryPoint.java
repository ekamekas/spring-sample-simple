package io.github.ekamekas.library.authentication.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.ekamekas.library.authentication.enums.AuthenticationMessageCode;
import io.github.ekamekas.library.core.dto.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class ServerAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private final HttpMessageConverter<String> messageConverter;
    private final ObjectMapper objectMapper;

    public ServerAuthenticationEntryPoint(ObjectMapper objectMapper) {
        this.messageConverter = new StringHttpMessageConverter();
        this.objectMapper = objectMapper;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        ServerHttpResponse serverResponse = new ServletServerHttpResponse(response);
        serverResponse.setStatusCode(HttpStatus.UNAUTHORIZED);

        messageConverter.write(
                objectMapper.writeValueAsString(
                        new ResponseDTO(AuthenticationMessageCode.MESSAGE_AUTHENTICATION_FAILED)
                ),
                MediaType.APPLICATION_JSON,
                serverResponse);
    }
}
