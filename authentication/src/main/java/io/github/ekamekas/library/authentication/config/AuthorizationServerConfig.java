package io.github.ekamekas.library.authentication.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;
    private final AuthenticationProperty property;

    public AuthorizationServerConfig(AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, @Qualifier("userService") UserDetailsService userDetailsService, AuthenticationProperty property) {
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsService = userDetailsService;
        this.property = property;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients
                .inMemory()
                .withClient(property.getOauth().get(Constant.Config.OAUTH_CLIENT_ID))
                .secret(passwordEncoder.encode(property.getOauth().get(Constant.Config.OAUTH_CLIENT_SECRET)))
                .accessTokenValiditySeconds(Integer.parseInt(property.getJwt().get(Constant.Config.JWT_TOKEN_VALIDITY)))
                .refreshTokenValiditySeconds(Integer.parseInt(property.getJwt().get(Constant.Config.JWT_REFRESH_TOKEN_VALIDITY)))
                .authorizedGrantTypes(property.getJwt().get(Constant.Config.JWT_AUTHORIZED_GRANT_TYPE).split(","))
                .scopes("read","write")
                .additionalInformation("organization:"+Constant.Token.TOKEN_ISSUER)
                .resourceIds("api");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
                .accessTokenConverter(accessTokenConverter())
                .userDetailsService(userDetailsService)
                .authenticationManager(authenticationManager);
    }

    @Bean
    JwtAccessTokenConverter accessTokenConverter(){
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        jwtAccessTokenConverter.setSigningKey(property.getJwt().get(Constant.Config.JWT_SECRET));
        return new JwtAccessTokenConverter();
    }
}
