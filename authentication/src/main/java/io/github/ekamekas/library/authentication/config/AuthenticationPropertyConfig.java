package io.github.ekamekas.library.authentication.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
public class AuthenticationPropertyConfig {

    @Configuration
    @PropertySource(value = "classpath:authentication.properties")
    static class DefaultProfile {}

    @Configuration
    @Profile("development")
    @PropertySource(value = "classpath:authentication-development.properties")
    static class DevelopmentProfile {}

    @Configuration
    @Profile("staging")
    @PropertySource(value = "classpath:authentication-staging.properties")
    static class StagingProfile {}

    @Configuration
    @Profile("production")
    @PropertySource(value = "classpath:authentication-production.properties")
    static class ProductionProfile {}

}
