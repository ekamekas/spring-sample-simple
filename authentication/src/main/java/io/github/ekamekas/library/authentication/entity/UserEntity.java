package io.github.ekamekas.library.authentication.entity;

import io.github.ekamekas.library.core.entity.BaseEntity;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
@Table(name = "USERS", indexes = {
        @Index(name = "IDX_USER", columnList = "username")
})
public class UserEntity extends BaseEntity {
    @NonNull
    @Column(nullable = false, unique = true)
    private String username;

    @NonNull
    @Column(nullable = false)
    private String password;

    @ManyToOne
    @NonNull
    private RoleEntity role;

    public UserEntity() {}

    public UserEntity(String username, String password, RoleEntity role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return super.toString() + '\n' +
                "UserEntity{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
