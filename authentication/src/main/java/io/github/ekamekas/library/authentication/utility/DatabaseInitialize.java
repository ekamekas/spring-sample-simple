package io.github.ekamekas.library.authentication.utility;

import io.github.ekamekas.library.authentication.config.Constant;
import io.github.ekamekas.library.authentication.entity.RoleEntity;
import io.github.ekamekas.library.authentication.entity.UserEntity;
import io.github.ekamekas.library.authentication.repository.RoleRepository;
import io.github.ekamekas.library.authentication.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DatabaseInitialize implements CommandLineRunner {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public DatabaseInitialize(RoleRepository roleRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        RoleEntity roleEntity = roleRepository.findByCode(Constant.Role.SUPER_ADMIN).orElseThrow();
        userRepository.save(
                new UserEntity("uR4n9peNt1NG",passwordEncoder.encode("n1nJ4Mu5tN0t93tC4ugHt"),roleEntity)
        );
    }
}
