package io.github.ekamekas.library.authentication.enums;

import io.github.ekamekas.library.core.enums.IMessageCode;

public enum AuthenticationMessageCode implements IMessageCode {
    MESSAGE_AUTHENTICATION_FAILED("XXX","Message authentication failed");

    private String code;
    private String message;

    AuthenticationMessageCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
