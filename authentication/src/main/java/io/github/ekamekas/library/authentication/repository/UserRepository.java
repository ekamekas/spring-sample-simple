package io.github.ekamekas.library.authentication.repository;

import io.github.ekamekas.library.authentication.entity.UserEntity;
import io.github.ekamekas.library.core.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<UserEntity> {
    Optional<UserEntity> findByUsername(String username);
}