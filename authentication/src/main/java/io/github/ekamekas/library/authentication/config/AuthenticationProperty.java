package io.github.ekamekas.library.authentication.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(prefix = "authentication")
public class AuthenticationProperty {
    private Map<String,String> oauth;
    private Map<String,String> jwt;

    public Map<String, String> getJwt() {
        return jwt;
    }

    public void setJwt(Map<String, String> jwt) {
        this.jwt = jwt;
    }

    public Map<String, String> getOauth() {
        return oauth;
    }

    public void setOauth(Map<String, String> oauth) {
        this.oauth = oauth;
    }
}
