package io.github.ekamekas.library.authentication.config;

public class Constant {

    /**
     * Properties config keys
     */
    public static class Config {
        // OAuth
        public static String OAUTH_CLIENT_ID = "clientId";
        public static String OAUTH_CLIENT_SECRET = "clientSecret";

        // JWT
        public static String JWT_SECRET = "secret";
        public static String JWT_TOKEN_VALIDITY = "tokenValidity";
        public static String JWT_REFRESH_TOKEN_VALIDITY = "refreshTokenValidity";
        public static String JWT_AUTHORIZED_GRANT_TYPE = "authorizedGrantType";
    }

    public static class Role {
        public static String SUPER_ADMIN = "SUPER_ADMIN";
    }

    public static class Token {
        public static String TOKEN_ISSUER = "Laman";
    }

}
