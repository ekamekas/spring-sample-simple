package io.github.ekamekas.library.authentication.repository;

import io.github.ekamekas.library.authentication.entity.RoleEntity;
import io.github.ekamekas.library.core.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends BaseRepository<RoleEntity> {
    Optional<RoleEntity> findByCode(String code);
}