CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Role
INSERT INTO role(created_by,created_date,last_modified_by,last_modified_date,secure_id,version,code,description,name)
VALUES('SYSTEM',CURRENT_TIMESTAMP,'SYSTEM',CURRENT_TIMESTAMP,uuid_generate_v4(),0,'SUPER_ADMIN','Role with superpowers','super admin');

-- Users
INSERT INTO users(created_by,created_date,last_modified_by,last_modified_date,secure_id,version,username,password,role_id)
VALUES('SYSTEM',CURRENT_TIMESTAMP,'SYSTEM',CURRENT_TIMESTAMP,uuid_generate_v4(),0,'uR4n9peNt1NG','n1nJ4Mu5tN0t93tC4ugHt',(select id from role where code='SUPER_ADMIN'));