# Authentication module
This module is part of laman spring boot library.

Module for authentication spring boot project.

Default credential :
* Username : uR4n9peNt1NG
* Password : n1nJ4Mu5tN0t93tC4ugHt

Default config :
* Authenticate for /api/**
* Permit for /api/public/**
* Permit else

## Technology stack
* [Spring security](https://spring.io/projects/spring-security)
* [JWT](https://jwt.io/)
* [Oauth2](https://oauth.net/2/)

## Module dependency
* [Core](https://github.com/ekamekas/spring-library-core)

## How to use
1. Put this module in the root project
2. Add dependency in the root project gradle