# Sample project
Sample project of spring boot application

## Technology stack
* [Spring boot](https://spring.io/projects/spring-boot)

## Module dependency
* [Core](https://github.com/ekamekas/spring-library-core)
* [Authentication](https://github.com/ekamekas/spring-library-authentication)
* [Application]()

## How to use
1. Run command
`./gradlew build && ./gradlew :application:bootRun`